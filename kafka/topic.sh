# insight-monitor
docker exec -it kafka kafka-topics.sh --create --topic counselor-login-input --zookeeper zookeeper:2181 --config delete.retention.ms=0 --config retention.ms=60000 --config segment.ms=60000 --partitions 3 --replication-factor 1
docker exec -it kafka kafka-topics.sh --create --topic counselor-login-trans --zookeeper zookeeper:2181 --config cleanup.policy=compact --config delete.retention.ms=0 --config retention.ms=60000 --config segment.ms=60000 --partitions 3 --replication-factor 1
docker exec -it kafka kafka-topics.sh --create --topic counselor-login-out --zookeeper zookeeper:2181 --config cleanup.policy=compact --config delete.retention.ms=0 --config retention.ms=60000 --config segment.ms=60000 --partitions 3 --replication-factor 1

# mocha-assignment
docker exec -it kafka kafka-topics.sh --create --topic retry-assignment --zookeeper zookeeper:2181 --config cleanup.policy=compact --config delete.retention.ms=0 --config retention.ms=60000 --config segment.ms=60000 --partitions 3 --replication-factor 1
docker exec -it kafka kafka-topics.sh --create --topic wait-for-retry-assignment --zookeeper zookeeper:2181 --config cleanup.policy=compact --config delete.retention.ms=0 --config retention.ms=60000 --config segment.ms=60000 --partitions 3 --replication-factor 1

#
docker exec -it kafka kafka-topics.sh --list --zookeeper zookeeper:2181
docker exec -it kafka kafka-topics.sh --describe --zookeeper zookeeper:2181 --topic retry-assignment

#
docker exec -it kafka kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic retry-assignment
docker exec -it kafka kafka-console-consumer.sh --bootstrap-server localhost:9092 --from-beginning --topic wait-for-retry-assignment
docker exec -it kafka kafka-console-producer.sh --broker-list localhost:9092 --topic retry-assignment


