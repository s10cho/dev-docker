#!/usr/bin/env bash
BROKER_SERVER=kafka1

for TOPIC in $(docker exec -it ${BROKER_SERVER} kafka-topics.sh --list --zookeeper zookeeper:2181|grep attic); do
    (
    cd ${TOPIC}
    docker exec -it ${BROKER_SERVER} kafka-topics.sh --delete --zookeeper zookeeper:2181 --topic ${TOPIC}
    )
done

docker exec -it ${BROKER_SERVER} kafka-topics.sh --list --zookeeper zookeeper:2181
